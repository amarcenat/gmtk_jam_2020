﻿using UnityEngine;

public class UseActionToggle : MonoBehaviour
{
    [SerializeField] private EActionType m_ActionType = EActionType.Invalid;
    [SerializeField] private SpriteRenderer m_IconSpriteRenderer;
    [SerializeField] private Sprite m_EnabledSprite;
    [SerializeField] private Sprite m_DisabledSprite;
    [SerializeField] private Camera m_Camera;
    private static Vector3 ms_SwitchOffset = new Vector3(0, -0.5f, 0);
    private bool m_IsUsed = false;
    private bool m_IsUsabled = true;
    private Animator m_Animator;

    public void OnEnable()
    {
        this.RegisterAsListener("Module", typeof(GameEvent_ActionEnabled), typeof(GameEvent_ActionDisabled));
        this.RegisterToUpdate(true, EUpdatePass.First);
        m_Animator = GetComponent<Animator>();
    }

    public void OnDisable()
    {
        this.UnregisterToUpdate(EUpdatePass.First);
        this.UnregisterAsListener("Module");
    }

    public void UpdateFirst()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 position = m_Camera.ScreenToWorldPoint(Input.mousePosition) - ms_SwitchOffset;
            if (Mathf.RoundToInt(position.x) == Mathf.RoundToInt(transform.position.x)
                && Mathf.RoundToInt(position.y) == Mathf.RoundToInt(transform.position.y))
            {
                ToggleUse();
            }
        }
    }

    public void ToggleUse()
    {
        if(!m_IsUsabled)
        {
            return;
        }
        if (!m_IsUsed)
        {
            BeginUse();
        }
        else
        { 
            StopUse();
        }
    }

    private void BeginUse()
    {
        if (!m_IsUsed)
        {
            m_IsUsed = true;
            m_Animator.SetFloat("IsUsed", 1);
            new GameEvent_ActionBeginUse(m_ActionType).Push();
        }
    }

    private void StopUse()
    {
        if (m_IsUsed)
        {
            new GameEvent_ActionStopUse(m_ActionType).Push();
            m_IsUsed = false;
            m_Animator.SetFloat("IsUsed", -1);
        }
    }

    public void OnGameEvent(GameEvent_ActionEnabled actionEnabled)
    {
        if (actionEnabled.GetActionType() == m_ActionType)
        {
            m_IconSpriteRenderer.sprite = m_EnabledSprite;
            m_IsUsabled = true;
        }
    }

    public void OnGameEvent(GameEvent_ActionDisabled actionDisabled)
    {
        if (actionDisabled.GetActionType() == m_ActionType)
        {
            StopUse();
            m_IconSpriteRenderer.sprite = m_DisabledSprite;
            m_IsUsabled = false;
        }
    }
}