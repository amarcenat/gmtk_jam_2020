﻿using UnityEngine;

public class PausePanel : MonoBehaviour
{
    [SerializeField] GameObject m_Panel;

    private void Awake()
    {
        m_Panel.SetActive(false);
        this.RegisterAsListener("Game", typeof(PauseEvent));
    }

    public void OnGameEvent(PauseEvent pause)
    {
        m_Panel.SetActive(pause.IsPaused());
    }

    private void OnDestroy()
    {
        this.UnregisterAsListener("Game");
    }
}
