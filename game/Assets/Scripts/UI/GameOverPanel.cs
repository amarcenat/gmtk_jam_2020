﻿using UnityEngine;

public class GameOverPanel : MonoBehaviour
{
    [SerializeField] GameObject m_Panel;
    [SerializeField] AudioClip m_GameOverMusic;

    private void Awake()
    {
        m_Panel.SetActive(false);
        this.RegisterAsListener("Game", typeof(GameOverGameEvent));
    }

    public void OnGameEvent(GameOverGameEvent gameOver)
    {
        m_Panel.SetActive(true);
        SoundManagerProxy.Get().StopAllMusic();
        SoundManagerProxy.Get().PlayMusic(m_GameOverMusic, 0);
    }

    private void OnDestroy()
    {
        this.UnregisterAsListener("Game");
    }
}
