﻿using UnityEngine;

public class HealthBar : MonoBehaviour
{
    [SerializeField] private Module m_Module;
    [SerializeField] private SpriteRenderer m_SpriteRenderer;
    [SerializeField] private Color m_NormalColor;
    [SerializeField] private Color m_RapidDegradationColor;
    [SerializeField] private Color m_RepairingColor;
    private Animator m_Animator;
    private Health m_Health;
    private float m_InitialXScale;
    private float m_CurrentReparationRate = 0;

    private void OnEnable()
    {
        m_InitialXScale = m_SpriteRenderer.transform.localScale.x;
        m_Health = m_Module.GetComponent<Health>();
        m_Animator = GetComponent<Animator>();
        m_SpriteRenderer.color = m_NormalColor;
        this.RegisterAsListener(m_Module.GetEventGUID()
            , typeof(DamageGameEvent)
            , typeof(GameEvent_BeginRepair)
            , typeof(GameEvent_StopRepair));
        this.RegisterAsListener("Module", typeof(GameEvent_ActionBeginUse), typeof(GameEvent_ActionStopUse));
    }

    private void OnDisable()
    {
        this.UnregisterAsListener("Module");
        this.UnregisterAsListener(m_Module.GetEventGUID());
    }

    public void OnGameEvent(DamageGameEvent damageEvent)
    {
        float originalValue = m_SpriteRenderer.bounds.min.x;
        
        float fraction = Mathf.Clamp01(m_Health.GetCurrentHealth() / m_Health.GetTotalHealth());
        m_SpriteRenderer.transform.localScale = new Vector3(fraction * m_InitialXScale, m_SpriteRenderer.transform.localScale.y, m_SpriteRenderer.transform.localScale.z);

        float newValue = m_SpriteRenderer.bounds.min.x;

        float difference = newValue - originalValue;
        m_SpriteRenderer.transform.Translate(new Vector3(-difference, 0f, 0f));
    }

    public void OnGameEvent(GameEvent_BeginRepair moduleEnabled)
    {
        m_CurrentReparationRate += 1;
        m_SpriteRenderer.color = m_RepairingColor;
        m_Animator.SetFloat("Repair", m_CurrentReparationRate);
    }

    public void OnGameEvent(GameEvent_StopRepair moduleDisabled)
    {
        m_CurrentReparationRate -= 1;
        m_SpriteRenderer.color = m_NormalColor;
        m_Animator.SetFloat("Repair", m_CurrentReparationRate);
    }

    public void OnGameEvent(GameEvent_ActionBeginUse moduleEnabled)
    {
        if (moduleEnabled.GetActionType() == m_Module.GetActionType())
        {
            m_SpriteRenderer.color = m_RapidDegradationColor;
        }
    }

    public void OnGameEvent(GameEvent_ActionStopUse moduleDisabled)
    {
        if (moduleDisabled.GetActionType() == m_Module.GetActionType())
        {
            m_SpriteRenderer.color = m_NormalColor;
        }
    }
}
