﻿using UnityEngine;

public class ModuleSmoke : MonoBehaviour
{
    [SerializeField] private Module m_Module;
    private Animator m_Animator;
    private bool m_IsDestroyed = false;

    private void OnEnable()
    {
        m_Animator = GetComponent<Animator>();
        this.RegisterAsListener(m_Module.GetEventGUID(), typeof(DamageGameEvent));
    }

    private void OnDisable()
    {
        this.UnregisterAsListener(m_Module.GetEventGUID());
    }

    public void OnGameEvent(DamageGameEvent damageEvent)
    {
        if (damageEvent.GetRemainingHealth() == 0)
        {
            m_IsDestroyed = true;
        }
        else
        {
            m_IsDestroyed = false;
        }
        m_Animator.SetBool("IsDestroyed", m_IsDestroyed);
    }
}
