﻿using UnityEngine;
using UnityEngine.UI;

public class ScoreText : MonoBehaviour
{
    [SerializeField] Score m_Score;
    private Text m_Text;

    private void Awake()
    {
        m_Text = GetComponent<Text>();
    }

    public void Update()
    {
        m_Text.text = m_Score.GetScore().ToString("F2");
    }
}
