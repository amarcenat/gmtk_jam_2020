﻿using System.Collections.Generic;
using UnityEngine;

public class ShipHealthBar : MonoBehaviour
{
    [SerializeField] private Health m_Health;
    [SerializeField] private GameObject m_HealthSlotPrefab;
    [SerializeField] private Sprite m_FilledSlot;
    [SerializeField] private Sprite m_EmptySlot;
    [SerializeField] private float m_Spacing = 20;
    private string m_ShipEventGUID;
    private List<SpriteRenderer> m_HealthSlotSprites = new List<SpriteRenderer>();

    private void OnEnable()
    {
        m_ShipEventGUID = m_Health.GetEventGUID();
        this.RegisterAsListener(m_ShipEventGUID, typeof(DamageGameEvent));
        int maxHP = (int)m_Health.GetMaxHealth();
        float ppu = 32;
        for(int i = 0; i < maxHP; ++i)
        {
            GameObject slot = Instantiate(m_HealthSlotPrefab, transform);
            Vector3 currentSlotPos = slot.transform.position;
            slot.transform.position = new Vector3(currentSlotPos.x + i*m_Spacing/ppu, currentSlotPos.y, currentSlotPos.z);
            m_HealthSlotSprites.Add(slot.GetComponent<SpriteRenderer>());
        }
    }

    private void OnDisable()
    {
        m_HealthSlotSprites.Clear();
        this.UnregisterAsListener(m_ShipEventGUID);
    }

    public void OnGameEvent(DamageGameEvent damageEvent)
    {
        int currentHP = (int)m_Health.GetCurrentHealth();
        for (int i = 0; i < m_HealthSlotSprites.Count; ++i)
        {
            SpriteRenderer sprite = m_HealthSlotSprites[i];
            if (i < currentHP)
            {
                sprite.sprite = m_FilledSlot;
            }
            else
            {
                sprite.sprite = m_EmptySlot;
            }
        }
    }
}
