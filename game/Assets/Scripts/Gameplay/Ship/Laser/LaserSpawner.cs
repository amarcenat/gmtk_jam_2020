﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserSpawner : MonoBehaviour
{
    // Position relative to the player
    [SerializeField] Transform m_SpawnPosition;
    [SerializeField] GameObject m_LaserPrefab;
    [SerializeField] float m_SpawnTime;
    [SerializeField] float m_ShipDefaultSpeed;
    private float m_Time;
    private bool m_IsShooting;
    private float m_ShipXSpeed = 0.0f;

    void Awake()
    {
        m_Time = 0.0f;
        m_IsShooting = false;
        this.RegisterAsListener("Module", typeof(GameEvent_ActionBeginUse), typeof(GameEvent_ActionStopUse));
        this.RegisterToUpdate(true, EUpdatePass.BeforeAI);
    }

    private void OnDestroy()
    {
        this.UnregisterToUpdate(EUpdatePass.BeforeAI);
        this.UnregisterAsListener("Module");
    }

    public void OnGameEvent(GameEvent_ActionBeginUse actionEvent)
    {
        EActionType actionType = actionEvent.GetActionType();
        switch (actionType)
        {
            case EActionType.GoLeft:
                m_ShipXSpeed += 1;
                break;
            case EActionType.GoRight:
                m_ShipXSpeed += -1;
                break;
            default:
                break;
        }
    }

    public void OnGameEvent(GameEvent_ActionStopUse actionEvent)
    {
        EActionType actionType = actionEvent.GetActionType();
        switch (actionType)
        {
            case EActionType.GoLeft:
                m_ShipXSpeed -= 1;
                break;
            case EActionType.GoRight:
                m_ShipXSpeed -= -1;
                break;
            default:
                break;
        }
    }

    public bool GetIsShooting()
    {
        return m_IsShooting;
    }

    public void SetIsShootingAndShoot(bool isShooting)
    {
        m_IsShooting = isShooting;

        if(m_IsShooting)
        {
            SpawnLaser();
        }
    }

    public void UpdateBeforeAI()
    {
        if (m_IsShooting)
        {
            m_Time += Time.deltaTime;
            if (m_Time > m_SpawnTime)
            {
                SpawnLaser();
                m_Time = 0.0f;
            }
        }

        if (m_ShipXSpeed != 0)
        {
            for (int i = 0; i < m_SpawnPosition.childCount; ++i)
            {
                GameObject laserObject = m_SpawnPosition.GetChild(i).gameObject;
                laserObject.GetComponent<Laser>().MoveOnX(m_ShipXSpeed);
            }
        }
    }

    private void SpawnLaser()
    {
        GameObject newLaser = Instantiate(m_LaserPrefab, m_SpawnPosition);
        newLaser.GetComponent<Laser>().SetupStartEnd(m_SpawnPosition.position);
    }
}
