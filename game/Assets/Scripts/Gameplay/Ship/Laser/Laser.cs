﻿using UnityEngine;

public class Laser : MonoBehaviour
{
    // Position relative to the player
    [SerializeField] float m_MovementTime = 3; // 3 seconds to go through the screen
    [SerializeField] float m_Range = 3;
    [SerializeField] AudioClip m_SpawnSound;

    private Vector3 m_StartPoint;
    private Vector3 m_EndPoint;
    private float m_LerpValue;

    private Vector3 m_StartingScale;
    private float m_AspirationTime = 0.0f;
    private bool m_IsAspirated = false;

    void Awake()
    {
        SoundManagerProxy.Get().PlayMultiple(m_SpawnSound);
        m_LerpValue = 0.0f;
        m_StartingScale = transform.localScale;
    }

    private void OnDestroy()
    {
    }
    
    public void Update()
    {
        m_LerpValue += Time.deltaTime / m_MovementTime;

        // Set our position as a fraction of the distance between the markers.
        transform.position = Vector3.Lerp(m_StartPoint, m_EndPoint, m_LerpValue);

        if (m_LerpValue > 1)
        {
            Destroy(gameObject);
        }

        if (m_IsAspirated)
        {
            m_AspirationTime += 0.25f;

            transform.localScale = Vector3.Lerp(m_StartingScale, new Vector3(0, 0), m_AspirationTime);
        }
    }

    public void MoveOnX(float speedShip)
    {
        m_StartPoint += new Vector3(Time.deltaTime * speedShip, 0, 0);
        m_EndPoint += new Vector3(Time.deltaTime * speedShip, 0, 0);
    }

    public void SetupStartEnd(Vector3 startPos)
    {
        m_StartPoint = startPos;
        m_EndPoint = startPos + new Vector3(0, m_Range, 0);
        transform.position = startPos;
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Hazard")
        {
            if (collision.gameObject.GetComponent<Hazard>().GetHazardType() == EHazardType.BlackHole)
            {
                m_IsAspirated = true;
                Destroy(gameObject, 1);
            }
        }
    }
}
