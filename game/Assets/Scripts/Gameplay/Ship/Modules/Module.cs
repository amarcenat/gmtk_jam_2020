﻿using UnityEngine;

[RequireComponent(typeof(Health))]
public class Module : TileObject
{
    [SerializeField] private EActionType m_ActionType = EActionType.Invalid;
    [SerializeField] private int m_RateScaling = 10;
    [SerializeField][Range(0.0f,10.0f)] private float m_NormalDegradationRate = 1.0f;
    [SerializeField][Range(0.0f,10.0f)] private float m_RapidDegradationRate = 2.0f;
    [SerializeField][Range(0.0f,10.0f)] private float m_ReparationRate = 2.0f;
    [SerializeField] private SpriteRenderer m_IconSprite;
    [SerializeField] AudioClip m_DisableSound;
    private Health m_Health;
    private float m_CurrentDegradationRate = 1.0f;
    private float m_CurrentReparationRate = 0.0f;
    private bool m_IsDisable = false;
    private bool m_IsBeingRepaired = false;

    public void OnEnable()
    {
        this.RegisterAsListener("Module"
            , typeof(GameEvent_ActionBeginUse)
            , typeof(GameEvent_ActionStopUse)
            , typeof(GameEvent_MagneticStorm)
            );
        this.RegisterAsListener(this.GetEventGUID(), typeof(GameEvent_BeginRepair), typeof(GameEvent_StopRepair));
        this.RegisterToUpdate(true, EUpdatePass.BeforeAI);
        m_Health = GetComponent<Health>();
        m_CurrentDegradationRate = 0; // m_NormalDegradationRate;
    }

    public void OnDisable()
    {
        this.UnregisterToUpdate(EUpdatePass.BeforeAI);
        this.UnregisterAsListener(this.GetEventGUID());
        this.UnregisterAsListener("Module");
    }

    private void DisableModule()
    {
        if (!m_IsDisable)
        {
            SoundManagerProxy.Get().PlayMultiple(m_DisableSound);
            m_IsDisable = true;
            new GameEvent_ModuleDisabled(m_ActionType).Push();
        }
    }

    private void EnableModule()
    {
        if (m_IsDisable)
        {
            m_IsDisable = false;
            new GameEvent_ModuleEnabled(m_ActionType).Push();
        }
    }

    public void OnGameEvent(GameEvent_ActionBeginUse actionEnabled)
    {
        if (actionEnabled.GetActionType() == m_ActionType)
        {
            m_CurrentDegradationRate = m_RapidDegradationRate;
            m_IconSprite.enabled = true;
        }
    }

    public void OnGameEvent(GameEvent_ActionStopUse actionDisabled)
    {
        if (actionDisabled.GetActionType() == m_ActionType)
        {
            m_IconSprite.enabled = false;
            m_CurrentDegradationRate = 0;// m_NormalDegradationRate;
        }
    }

    public void OnGameEvent(GameEvent_MagneticStorm magneticStorm)
    {
        LoseHealth(m_Health.GetMaxHealth()/2);
    }

    public void OnGameEvent(GameEvent_BeginRepair beginRepair)
    {
        m_IsBeingRepaired = true;
        m_CurrentReparationRate = m_ReparationRate;
        DisableModule();
    }

    public void OnGameEvent(GameEvent_StopRepair stopRepair)
    {
        m_CurrentReparationRate = 0.0f; 
        if (m_Health.GetCurrentHealth() > 0)
        {
            EnableModule();
        }
        m_IsBeingRepaired = false;
    }

    public void UpdateBeforeAI()
    {
        LoseHealth(-(m_CurrentReparationRate - m_CurrentDegradationRate) / m_RateScaling);
    }
    
    public void LoseHealth(float amount)
    {
        m_Health.LoseHealth(amount);
        if(m_Health.GetCurrentHealth() == 0)
        {
            DisableModule();
        }
        else if(!m_IsBeingRepaired)
        {
            EnableModule();
        }
    }

    public override bool IsObstacle()
    {
        return true;
    }

    public EActionType GetActionType()
    {
        return m_ActionType;
    }
}
