﻿using System.Collections.Generic;
using UnityEngine;

public class ModuleRepairTile : Tile
{
    [SerializeField] private Module m_Module;
    [SerializeField] private List<EWorkerClass> m_CompatibleWorkerClasses = new List<EWorkerClass>();

    public Module GetModule()
    {
        return m_Module;
    }

    public bool IsCompatibleWithClass(EWorkerClass workerClass) 
    {
        foreach (EWorkerClass wclass in m_CompatibleWorkerClasses)
        {
            if (workerClass == wclass)
            {
                return true;
            }
        }
        return false;
    }
}
