﻿public class GameEvent_ModuleBase : GameEvent
{
    public GameEvent_ModuleBase(EActionType actionType) : base("Module")
    {
        m_ActionType = actionType;
    }

    public EActionType GetActionType()
    {
        return m_ActionType;
    }

    private EActionType m_ActionType;
}

public class GameEvent_ModuleEnabled : GameEvent_ModuleBase
{
    public GameEvent_ModuleEnabled(EActionType actionType) : base(actionType)
    {
    }
}

public class GameEvent_ModuleDisabled : GameEvent_ModuleBase
{
    public GameEvent_ModuleDisabled(EActionType actionType) : base(actionType)
    {
    }
}

public class GameEvent_ActionBeginUse : GameEvent_ModuleBase
{
    public GameEvent_ActionBeginUse(EActionType actionType) : base(actionType)
    {
    }
}

public class GameEvent_ActionStopUse : GameEvent_ModuleBase
{
    public GameEvent_ActionStopUse(EActionType actionType) : base(actionType)
    {
    }
}

public class GameEvent_ActionDisabled : GameEvent_ModuleBase
{
    public GameEvent_ActionDisabled(EActionType actionType) : base(actionType)
    {
    }
}

public class GameEvent_ActionEnabled : GameEvent_ModuleBase
{
    public GameEvent_ActionEnabled(EActionType actionType) : base(actionType)
    {
    }
}

public class GameEvent_BeginRepair : GameEvent
{
    public GameEvent_BeginRepair(string tag) : base(tag)
    {
    }
}

public class GameEvent_StopRepair : GameEvent
{
    public GameEvent_StopRepair(string tag) : base(tag)
    {
    }
}