﻿using UnityEngine;

public class ShieldController : MonoBehaviour
{
    [SerializeField] AudioClip m_DeploySound;
    private Animator m_Animator;

    void Awake()
    {
        m_Animator = GetComponent<Animator>();
        this.RegisterAsListener("Module", typeof(GameEvent_ActionBeginUse), typeof(GameEvent_ActionStopUse));
    }

    private void OnDestroy()
    {
        this.UnregisterAsListener("Module");
    }

    public void OnGameEvent(GameEvent_ActionBeginUse actionEvent)
    {
        EActionType actionType = actionEvent.GetActionType();
        switch (actionType)
        {
            case EActionType.Shield:
                ToggleShield(true);
                break;
            default:
                break;
        }
    }

    public void OnGameEvent(GameEvent_ActionStopUse actionEvent)
    {
        EActionType actionType = actionEvent.GetActionType();
        switch (actionType)
        {
            case EActionType.Shield:
                ToggleShield(false);
                break;
            default:
                break;
        }
    }

    private void ToggleShield(bool value)
    {
        m_Animator.SetFloat("ShieldDeploy", value ? 1 : -1);
        if(value)
        {
            SoundManagerProxy.Get().PlayMultiple(m_DeploySound);
            new GameEvent_ShieldEnable().Push();
        }
        else
        {
            new GameEvent_ShieldDisable().Push();
        }
    }
}

