﻿using UnityEngine;

public class LaserController : MonoBehaviour
{
    [SerializeField] AudioClip m_FireSound;
    [SerializeField] LaserSpawner m_LaserSpawner;
    private Animator m_Animator;

    void Awake()
    {
        m_Animator = GetComponent<Animator>();
        this.RegisterAsListener("Module", typeof(GameEvent_ActionBeginUse), typeof(GameEvent_ActionStopUse));
    }

    private void OnDestroy()
    {
        this.UnregisterAsListener("Module");
    }

    public void OnGameEvent(GameEvent_ActionBeginUse actionEvent)
    {
        EActionType actionType = actionEvent.GetActionType();
        switch (actionType)
        {
            case EActionType.Laser:
                ToggleLaser(true);
                break;
            default:
                break;
        }
    }

    public void OnGameEvent(GameEvent_ActionStopUse actionEvent)
    {
        EActionType actionType = actionEvent.GetActionType();
        switch (actionType)
        {
            case EActionType.Laser:
                ToggleLaser(false);
                break;
            default:
                break;
        }
    }

    private void ToggleLaser(bool value)
    {
        m_Animator.SetFloat("LaserDeploy", value ? 1 : -1);
        m_LaserSpawner.SetIsShootingAndShoot(value);
    }
}

