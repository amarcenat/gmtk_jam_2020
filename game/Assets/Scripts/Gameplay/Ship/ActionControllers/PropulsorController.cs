﻿using UnityEngine;

public class PropulsorController : MonoBehaviour
{
    [SerializeField] AudioClip m_PropulsorSound;
    [SerializeField] EActionType m_ActionType;
    private Animator m_Animator;

    void Awake()
    {
        m_Animator = GetComponent<Animator>();
        this.RegisterAsListener("Module", typeof(GameEvent_ActionBeginUse), typeof(GameEvent_ActionStopUse));
    }

    private void OnDestroy()
    {
        this.UnregisterAsListener("Module");
    }

    public void OnGameEvent(GameEvent_ActionBeginUse actionEvent)
    {
        EActionType actionType = actionEvent.GetActionType();
        if (actionType == m_ActionType)
        {
            TogglePropulsor(true);
        }
    }

    public void OnGameEvent(GameEvent_ActionStopUse actionEvent)
    {
        EActionType actionType = actionEvent.GetActionType();
        if (actionType == m_ActionType)
        {
            TogglePropulsor(false);
        }
    }

    private void TogglePropulsor(bool value)
    {
        m_Animator.SetFloat("PropulsorDeploy", value ? 1 : -1);
        SoundManagerProxy.Get().PlayMultiple(m_PropulsorSound);
    }
}

