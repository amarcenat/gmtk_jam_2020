﻿using System.Collections;
using UnityEngine;

public class MovingTileObject : TileObject
{
    [SerializeField] private float m_MoveSpeed = 10f;
    private bool m_IsMoving = false;
    private Vector3 m_TargetPos;

    public void BeginMovement(TileCoordinates targetCoordinate)
    {
        SetTargetPos(targetCoordinate);
        StartCoroutine(MoveRoutine());
    }

    public bool GetIsMoving()
    {
        return m_IsMoving;
    }

    private void StopMovement()
    {
        if (m_IsMoving)
        {
            StopAllCoroutines();
            SetIsMoving(false);
            transform.position = m_TargetPos;
        }
    }

    private void SetTargetPos(TileCoordinates targetCoordinate)
    {
        m_TargetPos = new Vector3(targetCoordinate.x, targetCoordinate.y, transform.position.z);
    }

    private void SetIsMoving(bool isMoving)
    {
        m_IsMoving = isMoving;
    }

    IEnumerator MoveRoutine()
    {
        SetIsMoving(true);
        while (transform.position != m_TargetPos)
        {
            transform.position = Vector3.MoveTowards(transform.position, m_TargetPos, Time.deltaTime * m_MoveSpeed);
            yield return null;
        }
        SnapOnCurrentCoordinates();
        SetIsMoving(false);
    }

    private void SnapOnCurrentCoordinates()
    {
        TileCoordinates currentCoordinates = GetCoordinates();
        Vector3 currentPosition = new Vector3(currentCoordinates.x, currentCoordinates.y, 0);
        transform.position = currentPosition;
    }
}