﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileNode
{
    public TileNode(Tile tile, TileNode parent, TileCoordinates startPos, TileCoordinates endPos)
    {
        this.m_tile = tile;
        this.m_parent = parent;
        TileCoordinates tilePos = tile.GetCoordinates();
        m_quality = TileCoordinates.Distance(startPos, tilePos) + TileCoordinates.Distance(tilePos, endPos);
    }

    public Tile m_tile;
    public TileNode m_parent;
    public int m_quality;
}