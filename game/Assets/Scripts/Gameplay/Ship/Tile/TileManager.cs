﻿using System.Collections.Generic;
using UnityEngine.Assertions;

public class TileManager : ITileManager
{
    private Dictionary<TileCoordinates, Tile> m_Tiles;
    private Dictionary<TileCoordinates, TileObject> m_TileObjects;
    private TileAstar m_TileAstar;

    public TileManager()
    {
        m_Tiles = new Dictionary<TileCoordinates, Tile>();
        m_TileObjects = new Dictionary<TileCoordinates, TileObject>();
        m_TileAstar = new TileAstar();
    }

    public Tile GetTile(int x, int y)
    {
        return GetTile(new TileCoordinates(x, y));
    }

    public Tile GetTile(TileCoordinates coordinates)
    {
        Tile tile = null;
        m_Tiles.TryGetValue(coordinates, out tile);
        return tile;
    }

    public void AddTile(Tile tile)
    {
        TileCoordinates coordinates = tile.GetCoordinates();
        Assert.IsTrue(GetTile(coordinates) == null, string.Format("Several tile at coordinates x:{0}, y:{1}", coordinates.x, coordinates.y));
        m_Tiles.Add(coordinates, tile);
    }
    public void AddTileObject(TileObject tileObject)
    {
        m_TileObjects.Add(tileObject.GetCoordinates(), tileObject);
    }

    public void RemoveTileObject(TileObject tileObject)
    {
        m_TileObjects.Remove(tileObject.GetCoordinates());
    }

    public TileObject GetObjectInTile(int x, int y)
    {
        return GetObjectInTile(new TileCoordinates(x, y));
    }

    public TileObject GetObjectInTile(TileCoordinates coordinates)
    {
        TileObject tileObject = null;
        m_TileObjects.TryGetValue(coordinates, out tileObject);
        return tileObject;
    }

    public void Reset()
    {
        m_Tiles.Clear();
        m_TileObjects.Clear();
    }

    public List<Tile> GetTilePath(Tile start, Tile end)
    {
        return m_TileAstar.ComputePath(start, end);
    }
}