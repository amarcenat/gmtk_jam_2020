﻿using System.Collections.Generic;

public interface ITileManager
{
    Tile GetTile(int x, int y);
    Tile GetTile(TileCoordinates coordinates);
    void AddTile(Tile tile);
    void AddTileObject(TileObject tileObject);
    void RemoveTileObject(TileObject tileObject);
    TileObject GetObjectInTile(int x, int y);
    TileObject GetObjectInTile(TileCoordinates coordinates);
    void Reset();
    List<Tile> GetTilePath(Tile start, Tile end);
}

public class TileManagerProxy : UniqueProxy<ITileManager>
{ }