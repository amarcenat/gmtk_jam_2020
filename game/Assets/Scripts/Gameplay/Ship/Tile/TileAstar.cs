﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TileAstar
{
    private List<TileNode> m_toVisit = new List<TileNode>();
    private List<TileNode> m_visited = new List<TileNode>();

    public TileAstar()
    {
    }

    private bool HasBeenVisited(TileNode node)
    {
        foreach(TileNode visitedNode in m_visited)
        {
            if(visitedNode.m_tile == node.m_tile)
            {
                return true;
            }
        }

        return false;
    }

    public List<Tile> ComputePath(Tile start, Tile end)
    {
        TileNode currentTile = new TileNode(start, null, start.GetCoordinates(), end.GetCoordinates());
        m_toVisit.Clear();
        m_visited.Clear();

        while (currentTile.m_tile != end)
        {
            m_visited.Add(currentTile);

            // Add all available and not visited neighbor
            currentTile.m_tile.BuildNeighboorList();
            foreach (Tile neighbor in currentTile.m_tile.GetNeighboorList())
            {
                TileNode neighborNode = new TileNode(neighbor, currentTile, start.GetCoordinates(), end.GetCoordinates());
                if(!HasBeenVisited(neighborNode))
                {
                    m_toVisit.Add(neighborNode);
                }
            }

            // If we can't go further, we stop, no path are available
            if(m_toVisit.Count == 0)
            {
                return null;
            }

            // The next currentTile is the one with the best quality
            m_toVisit = m_toVisit.OrderBy(node => node.m_quality).ToList<TileNode>();
            currentTile = m_toVisit[0];
            m_toVisit.RemoveAt(0);
        }

        List<Tile> path = new List<Tile>();
        TileNode resultNode = currentTile;
        while (resultNode != null && resultNode.m_tile != start)
        {
            path.Add(resultNode.m_tile);
            resultNode = resultNode.m_parent;
        }
        path.Reverse();
        return path;
    }
}
