﻿using UnityEngine;

public enum ETileObjectType
{
    None
    , Worker
    , Module
}

public class TileObject : MonoBehaviour
{
    [SerializeField] private ETileObjectType m_Type;
    private TileCoordinates m_Coordinates;

    public void Start()
    {
        int x = (int)transform.position.x;
        int y = (int)transform.position.y;
        m_Coordinates = new TileCoordinates(x, y);
        TileManagerProxy.Get().AddTileObject(this);
    }

    public void Awake()
    {
    }

    public void OnDestroy()
    {
    }

    public ETileObjectType GetObjectType()
    {
        return m_Type;
    }

    public void SetObjectType(ETileObjectType type)
    {
        m_Type = type;
    }

    public TileCoordinates GetCoordinates()
    {
        return m_Coordinates;
    }

    public void SetCoordinates(TileCoordinates coordinates)
    {
        m_Coordinates = coordinates;
    }

    public Tile GetTile()
    {
        return TileManagerProxy.Get().GetTile(GetCoordinates());
    }

    public virtual bool IsObstacle()
    {
        return false;
    }
}