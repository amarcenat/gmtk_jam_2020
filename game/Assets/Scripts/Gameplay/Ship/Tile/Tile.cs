﻿using System.Collections.Generic;
using UnityEngine;

public enum ETileType
{
    Invalid
    , Normal
    , Wall
    , RepairModule
}

public enum EDirection
{
    Right,
    Up,
    Left,
    Down
}

public class Tile : MonoBehaviour
{
    [SerializeField] private ETileType m_Type;
    [SerializeField] private bool m_IsObstacle = false;
    private TileCoordinates m_Coordinates;
    private List<Tile> m_Neighboors = new List<Tile>();

    public void Start()
    {
        int x = (int)transform.position.x;
        int y = (int)transform.position.y;
        m_Coordinates = new TileCoordinates(x, y);
        TileManagerProxy.Get().AddTile(this);
    }

    public void Awake()
    {
    }

    public void OnDestroy()
    {
    }

    public bool IsObstacle() { return m_IsObstacle; }

    public TileCoordinates GetCoordinates()
    {
        return m_Coordinates;
    }

    public void SetCoordinates(TileCoordinates coordinates)
    {
        m_Coordinates = coordinates;
    }

    public ETileType GetTileType()
    {
        return m_Type;
    }

    public void SetTileType(ETileType type)
    {
        m_Type = type;
    }

    public TileObject GetTileObject()
    {
        return TileManagerProxy.Get().GetObjectInTile(GetCoordinates());
    }

    public List<Tile> GetNeighboorList()
    {
        return m_Neighboors;
    }

    public void BuildNeighboorList()
    {
        m_Neighboors.Clear();
        TileCoordinates coordinates = m_Coordinates;
        foreach(TileCoordinates direction in ms_DirectionTileVector.Values)
        {
            TileCoordinates neighBoorCoordinates = m_Coordinates + direction;
            Tile neighBoorTile = TileManagerProxy.Get().GetTile(neighBoorCoordinates);
            if(neighBoorTile != null && !neighBoorTile.IsObstacle())
            {
                m_Neighboors.Add(neighBoorTile);
            }
        }
    }

    public static Dictionary<EDirection, TileCoordinates> ms_DirectionTileVector = new Dictionary<EDirection, TileCoordinates>()
    {
        { EDirection.Right, new TileCoordinates(1, 0) },
        { EDirection.Left,  new TileCoordinates(-1, 0) },
        { EDirection.Up,    new TileCoordinates(0, 1) },
        { EDirection.Down,  new TileCoordinates(0, -1) },
    };
}