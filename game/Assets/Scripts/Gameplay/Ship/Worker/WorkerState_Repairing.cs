﻿
using UnityEngine;

public class WorkerState_Repairing : WorkerState
{
    private Module m_Module;

    public WorkerState_Repairing(Worker worker) : base(worker)
    {
    }

    public override void OnEnter()
    {
        this.RegisterAsListener(m_Worker.GetEventGUID(), typeof(GameEvent_WorkerMoveRequest));
        Tile currentTile = m_Worker.GetTile();
        ModuleRepairTile repairTile = currentTile as ModuleRepairTile;
        m_Module = repairTile.GetModule();
        m_Worker.GetComponent<Animator>().SetBool("Angry", false);
        m_Worker.GetComponent<Animator>().SetBool("Repairing", true);
        new GameEvent_BeginRepair(m_Module.GetEventGUID()).Push();
    }

    public override void OnExit()
    {
        new GameEvent_StopRepair(m_Module.GetEventGUID()).Push();
        m_Worker.GetComponent<Animator>().SetBool("Repairing", false);
        this.UnregisterAsListener(m_Worker.GetEventGUID());
    }

    public void OnGameEvent(GameEvent_WorkerMoveRequest moveRequest)
    {
        ChangeNextTransition(HSMTransition.EType.Clear, typeof(WorkerState_Moving));
        m_Worker.SetTargetTile(moveRequest.GetTargetTile());
    }
}