﻿public class GameEvent_WorkerMoveRequest : GameEvent
{
    public GameEvent_WorkerMoveRequest(string tag, Tile target) : base(tag)
    {
        m_Target = target;
    }

    public Tile GetTargetTile()
    {
        return m_Target;
    }

    private Tile m_Target;
}