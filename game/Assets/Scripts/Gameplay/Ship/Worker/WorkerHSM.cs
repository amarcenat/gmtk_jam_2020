﻿public class WorkerState : HSMState
{
    protected Worker m_Worker;

    public WorkerState(Worker worker)
    {
        m_Worker = worker;
    }
}

public class WorkerHSM : HSM
{
    public WorkerHSM(Worker worker)
        : base(new WorkerState_Idle(worker)
              , new WorkerState_Repairing(worker)
              , new WorkerState_Moving(worker)
        )
    {
    }

    public void StartFlow()
    {
        Start(typeof(WorkerState_Idle));
        this.RegisterToUpdate(false, EUpdatePass.Last);
    }

    public void StopFlow()
    {
        this.UnregisterToUpdate(EUpdatePass.Last);
        Stop();
    }
}