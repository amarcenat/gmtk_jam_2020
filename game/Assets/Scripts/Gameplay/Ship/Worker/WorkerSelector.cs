﻿using UnityEngine;

public class WorkerSelector : MonoBehaviour
{
    public Camera m_camera;
    private Worker m_selectedWorker = null;
    private static Vector3 ms_WorkerOffset = new Vector3(0, 0.5f, 0);
    public void OnEnable()
    {
        this.RegisterToUpdate(true, EUpdatePass.First);
    }

    public void OnDisable()
    {
        this.UnregisterToUpdate(EUpdatePass.First);
    }

    public void UpdateFirst()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if(m_selectedWorker != null)
            {
                m_selectedWorker.OnDeselected();
            }
            m_selectedWorker = GetWorker(Input.mousePosition);
            if (m_selectedWorker != null)
            {
                m_selectedWorker.OnSelected();
            }
        }
        else if (m_selectedWorker != null && Input.GetMouseButtonDown(1)) /* check if we click on a tile to set the destination */
        {
            Tile destination = GetDestination(Input.mousePosition);
            if(destination)
            {
                new GameEvent_WorkerMoveRequest(m_selectedWorker.GetEventGUID(), destination).Push();
            }
        }
    }

    private void Reset()
    {
        m_selectedWorker = null;
    }

    private Worker GetWorker(Vector3 mousePosition)
    {
        Vector3 position = m_camera.ScreenToWorldPoint(mousePosition) - ms_WorkerOffset;
        TileObject selectedObject = TileManagerProxy.Get().GetObjectInTile(Mathf.RoundToInt(position.x), Mathf.RoundToInt(position.y));
        if(selectedObject)
        {
            if(selectedObject.GetObjectType() == ETileObjectType.Worker)
            {
                return selectedObject as Worker;
            }
        }
        return null;
    }

    private Tile GetDestination(Vector3 mousePosition)
    {
        Vector3 position = m_camera.ScreenToWorldPoint(mousePosition);
        Tile selectedTile = TileManagerProxy.Get().GetTile(Mathf.RoundToInt(position.x), Mathf.RoundToInt(position.y));
        if (selectedTile && !selectedTile.IsObstacle())
        {
            return selectedTile;
        }
        return null;
    }
}
