﻿
using UnityEngine;

public enum EWorkerClass
{
    Invalid
    , Engineer
    , Scientist
};

public class Worker : MovingTileObject
{
    [SerializeField] private EWorkerClass m_Class = EWorkerClass.Invalid;
    [SerializeField] private Material m_OutlineMaterial;
    [SerializeField] private Material m_DefaultMaterial;
    [SerializeField] private SpriteRenderer m_SpriteRenderer;
    private WorkerHSM m_HSM;
    private Tile m_TargetTile;

    public void OnEnable()
    {
        m_HSM = new WorkerHSM(this);
        m_HSM.StartFlow();
    }

    public void OnDisable()
    {
        m_HSM.StopFlow();
    }

    public EWorkerClass GetClass()
    {
        return m_Class;
    }

    public override bool IsObstacle()
    {
        return true;
    }

    public Tile GetTargetTile()
    {
        return m_TargetTile;
    }

    public void SetTargetTile(Tile targetTile)
    {
        m_TargetTile = targetTile;
    }

    public void OnSelected()
    {
        m_SpriteRenderer.material = m_OutlineMaterial;
    }

    public void OnDeselected()
    {
        m_SpriteRenderer.material = m_DefaultMaterial;
    }
}
