﻿
using UnityEngine;

public class WorkerState_Idle : WorkerState
{
    public WorkerState_Idle(Worker worker) : base(worker)
    {
    }

    public override void OnEnter()
    {
        this.RegisterAsListener(m_Worker.GetEventGUID(), typeof(GameEvent_WorkerMoveRequest));
    }

    public override void OnExit()
    {
        this.UnregisterAsListener(m_Worker.GetEventGUID());
    }

    public void OnGameEvent(GameEvent_WorkerMoveRequest moveRequest)
    {
        ChangeNextTransition(HSMTransition.EType.Clear, typeof(WorkerState_Moving));
        m_Worker.SetTargetTile(moveRequest.GetTargetTile());
    }
}
