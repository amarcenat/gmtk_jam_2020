﻿
using System.Collections.Generic;
using UnityEngine;

public class WorkerState_Moving : WorkerState
{
    private Tile m_Start;
    private Tile m_End;
    private List<Tile> m_Path = new List<Tile>();
    private int m_PathIndex;

    public WorkerState_Moving(Worker worker) : base(worker)
    {
    }

    public override void OnEnter()
    {
        this.RegisterAsListener(m_Worker.GetEventGUID(), typeof(GameEvent_WorkerMoveRequest));
        this.RegisterToUpdate(true, EUpdatePass.AI);
        BeginMove();
        m_Worker.GetComponent<Animator>().SetBool("Angry", false);
        m_Worker.GetComponent<Animator>().SetBool("Moving", true);
    }

    public override void OnExit()
    {
        m_Path = null;
        m_Worker.GetComponent<Animator>().SetBool("Moving", false);
        this.UnregisterToUpdate(EUpdatePass.AI);
        this.UnregisterAsListener(m_Worker.GetEventGUID());
    }

    private void BeginMove()
    {
        m_Start = m_Worker.GetTile();
        m_End = m_Worker.GetTargetTile();
        m_Path = TileManagerProxy.Get().GetTilePath(m_Start, m_End);
        m_PathIndex = 0;
        if (m_Path == null)
        {
            WrongMove();
        }
    }

    private void WrongMove()
    {
        ChangeNextTransition(HSMTransition.EType.Clear, typeof(WorkerState_Idle));
    }

    public void OnGameEvent(GameEvent_WorkerMoveRequest moveRequest)
    {
        m_Worker.SetTargetTile(moveRequest.GetTargetTile());
        BeginMove();
    }

    public void UpdateAI()
    {
        if(m_Worker.GetIsMoving())
        {
            return;
        }

        if (m_PathIndex == m_Path.Count)
        {
            m_PathIndex = 0;
            Tile currentTile = m_Worker.GetTile();
            ModuleRepairTile repairTile = currentTile as ModuleRepairTile;
            if (repairTile != null)
            {
                if (repairTile.IsCompatibleWithClass(m_Worker.GetClass()))
                {
                    ChangeNextTransition(HSMTransition.EType.Clear, typeof(WorkerState_Repairing));
                }
                else
                {
                    m_Worker.GetComponent<Animator>().SetBool("Angry", true);
                    ChangeNextTransition(HSMTransition.EType.Clear, typeof(WorkerState_Idle));
                }
            }
            else
            {
                ChangeNextTransition(HSMTransition.EType.Clear, typeof(WorkerState_Idle));
            }
        }
        else
        {
            Tile nextTile = m_Path[m_PathIndex];
            TileObject tileObject = nextTile.GetTileObject();
            if (tileObject != null && tileObject.IsObstacle())
            {
                WrongMove();
            }
            else
            {
                TileCoordinates targetCoordinates = nextTile.GetCoordinates();

                TileManagerProxy.Get().RemoveTileObject(m_Worker);
                m_Worker.SetCoordinates(targetCoordinates);
                TileManagerProxy.Get().AddTileObject(m_Worker);

                m_Worker.BeginMovement(targetCoordinates);
                m_PathIndex++;
            }
        }
    }
}
