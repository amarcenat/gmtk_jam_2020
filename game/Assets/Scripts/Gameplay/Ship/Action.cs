﻿using UnityEngine.Assertions;

public enum EActionType
{
    Invalid
    , Shield
    , Laser
    , GoLeft
    , GoRight
};

public class Action
{
    private EActionType m_Type = EActionType.Invalid;
    private uint m_DeactivatedModules = 0;

    public void Init(EActionType type)
    {
        m_Type = type;
        this.RegisterAsListener("Module", typeof(GameEvent_ModuleEnabled), typeof(GameEvent_ModuleDisabled));
    }

    public void Shutdown()
    {
        this.UnregisterAsListener("Module");
    }

    public bool IsEnabled()
    {
        return m_DeactivatedModules == 0;
    }

    public EActionType GetActionType()
    {
        return m_Type;
    }

    public void OnGameEvent(GameEvent_ModuleEnabled moduleEnabled)
    {
        if (moduleEnabled.GetActionType() == m_Type)
        {
            if (m_DeactivatedModules > 0)
            {
                m_DeactivatedModules--;
                if(m_DeactivatedModules == 0)
                {
                    new GameEvent_ActionEnabled(m_Type).Push();
                }
            }
            else
            {
                Assert.IsTrue(false, "Received inconsistent number of module event");
            }
        }
    }

    public void OnGameEvent(GameEvent_ModuleDisabled moduleDisabled)
    {
        if (moduleDisabled.GetActionType() == m_Type)
        {
            if(m_DeactivatedModules == 0)
            {
                new GameEvent_ActionDisabled(m_Type).Push();
            }
            m_DeactivatedModules++;
        }
    }
}