﻿using UnityEngine;

public class DamageGameEvent : GameEvent
{
    public DamageGameEvent(string tag, float damage, float remainingHealth) : base(tag, EProtocol.Instant)
    {
        m_Damage = damage;
        m_RemainingHealth = remainingHealth;
    }

    public float GetDamage()
    {
        return m_Damage;
    }

    public float GetRemainingHealth()
    {
        return m_RemainingHealth;
    }

    private float m_Damage;
    private float m_RemainingHealth;
}

public class Health : MonoBehaviour
{
    [SerializeField] private float m_MaxHealth;
    private float m_CurrentHealth;
    private bool m_Enable = true;

    public void Start()
    {
        m_CurrentHealth = m_MaxHealth;
    }

    public void Heal(float value)
    {
        if (!m_Enable)
        {
            return;
        }
        float newHealth = Mathf.Min(m_MaxHealth, m_CurrentHealth + value);
        float heal = newHealth - m_CurrentHealth;
        m_CurrentHealth = newHealth;
        PushDamageEvent(-heal);
    }

    public void FullHeal()
    {
        if (!m_Enable)
        {
            return;
        }
        float heal = m_MaxHealth - m_CurrentHealth;
        m_CurrentHealth = m_MaxHealth;
        PushDamageEvent(-heal);
    }

    public void LoseHealth(float damage)
    {
        if (!m_Enable)
        {
            return;
        }

        m_CurrentHealth = Mathf.Max(0, m_CurrentHealth - damage);
        if(m_CurrentHealth > m_MaxHealth)
        {
            m_CurrentHealth = m_MaxHealth;
        }
        PushDamageEvent(damage);
    }

    public void LoseMaxHealth(int newMaxHealth)
    {
        if (!m_Enable)
        {
            return;
        }

        float damage = m_MaxHealth - newMaxHealth;
        m_MaxHealth = Mathf.Max(0, newMaxHealth);
        m_CurrentHealth = Mathf.Min(0, m_CurrentHealth - damage);
        PushDamageEvent(damage);
    }

    public float GetCurrentHealth()
    {
        return m_CurrentHealth;
    }

    public float GetMaxHealth()
    {
        return m_MaxHealth;
    }

    public void SetMaxHealth(float value)
    {
        m_MaxHealth = value;
        FullHeal();
    }

    public float GetTotalHealth()
    {
        return m_MaxHealth;
    }

    public void Enable(bool enable)
    {
        m_Enable = enable;
    }

    private void PushDamageEvent(float damage)
    {
        new DamageGameEvent(this.GetEventGUID(), damage, m_CurrentHealth).Push();
    }
}
