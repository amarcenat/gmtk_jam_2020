﻿using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System.Collections;

public class GameEvent_ShipDamage : GameEvent
{
    public GameEvent_ShipDamage(int damage) : base("Ship")
    {
        m_Damage = damage;
    }

    public int GetDamage()
    {
        return m_Damage;
    }

    private int m_Damage;
}

public class GameEvent_ShieldEnable : GameEvent
{
    public GameEvent_ShieldEnable() : base("Ship")
    {
    }
}

public class GameEvent_ShieldDisable : GameEvent
{
    public GameEvent_ShieldDisable() : base("Ship")
    {
    }
}

public class Ship : MonoBehaviour
{
    [SerializeField] SpriteRenderer m_SpriteRendererShmup;
    [SerializeField] float m_InvulnerabilityTime;
    [SerializeField] float m_ColorIntervalTime;
    [SerializeField] AudioClip m_HitSound;
    [SerializeField] Image m_RedPanelImage;

    [SerializeField] private List<EActionType> m_AvailableActions = new List<EActionType>();
    [SerializeField] private Animator m_ShipAnimator;
    private List<Action> m_Actions = new List<Action>();
    private Health m_Health;
    private bool m_ShieldEnable;
    private bool m_IsInvulnerable;
    private float m_TimeInvulnerable;
    private float m_TimeColor;
    private bool m_IsWhite = true;
    private bool m_HasRedPanelTickled = false;

    public void OnEnable()
    {
        m_Health = GetComponent<Health>();
        m_IsInvulnerable = false;
        m_ShieldEnable = false;
        m_TimeInvulnerable = 0.0f;
        m_TimeColor = 0.0f;
        m_Actions.Clear();
        foreach (EActionType actionType in m_AvailableActions)
        {
            AddAction(actionType);
        }
        this.RegisterAsListener("Ship", typeof(GameEvent_ShipDamage));
        this.RegisterAsListener("Ship", typeof(GameEvent_ShieldEnable));
        this.RegisterAsListener("Ship", typeof(GameEvent_ShieldDisable));
    }

    public void OnDisable()
    {
        this.UnregisterAsListener("Ship");
        foreach (Action action in m_Actions)
        {
            action.Shutdown();
        }
        m_Actions.Clear();
    }

    private void AddAction(EActionType actionType)
    {
        Action action = new Action();
        action.Init(actionType);
        m_Actions.Add(action);
    }

    private Action GetAction(EActionType actionType)
    {
        Action res = null;
        foreach (Action action in m_Actions)
        {
            if (action.GetActionType() == actionType)
            {
                res = action;
                break;
            }
        }
        return res;
    }

    public void OnGameEvent(GameEvent_ShipDamage shipDamage)
    {
        if(m_ShieldEnable || m_IsInvulnerable)
        {
            return;
        }

        SoundManagerProxy.Get().PlayMultiple(m_HitSound);
        m_Health.LoseHealth(shipDamage.GetDamage());
        m_IsInvulnerable = true;
        m_IsWhite = false;
        m_SpriteRendererShmup.color = new Color(255, 0, 0);
        m_RedPanelImage.color += new Color(0, 0, 0, 0.5f);
        if(m_Health.GetCurrentHealth() == 0)
        {
            StartCoroutine(WaitDeath());
        }
    }

    private IEnumerator WaitDeath()
    {
        m_ShipAnimator.SetTrigger("Death");
        yield return new WaitForSecondsRealtime(1);
        new GameFlowEvent(EGameFlowAction.GameOver).Push();
    }

    public void OnGameEvent(GameEvent_ShieldEnable shieldState)
    {
        m_ShieldEnable = true;
    }

    public void OnGameEvent(GameEvent_ShieldDisable shieldState)
    {
        m_ShieldEnable = false;
    }

    public void Update()
    {
        if(m_IsInvulnerable)
        {
            m_TimeInvulnerable += Time.deltaTime;
            m_TimeColor += Time.deltaTime;
            if(m_TimeColor >= m_ColorIntervalTime)
            {
                if (m_IsWhite)
                {
                    m_SpriteRendererShmup.color = new Color(255, 0, 0, 255);
                    m_IsWhite = false;
                }
                else
                {
                    m_SpriteRendererShmup.color = new Color(255, 255, 255, 255);
                    m_IsWhite = true;

                    if(m_RedPanelImage.color.a != 0)
                    {
                        m_RedPanelImage.color -= new Color(0, 0, 0, 0.5f);
                    }
                }

                m_TimeColor = 0.0f;
            }

            if (m_TimeInvulnerable >= m_InvulnerabilityTime)
            {
                m_IsInvulnerable = false;
                m_IsWhite = true;
                m_TimeInvulnerable = 0.0f;
                m_SpriteRendererShmup.color = new Color(255, 255, 255);

            }
        }
    }
}
