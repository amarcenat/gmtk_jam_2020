﻿
public class GameFlowNormalState : HSMState
{
    public override void OnEnter ()
    {
        LevelManagerProxy.Get ().LoadScene (1);
        this.RegisterAsListener ("Player", typeof (PlayerInputGameEvent));
        this.RegisterAsListener("Game", typeof(GameFlowEvent));
        TileManagerProxy.Get().Reset();
    }

    public void OnGameEvent (PlayerInputGameEvent inputEvent)
    {
        if (inputEvent.GetInput () == "Pause" && inputEvent.GetInputState() == EInputState.Down && !UpdaterProxy.Get().IsPaused())
        {
            ChangeNextTransition (HSMTransition.EType.Child, typeof (GameFlowPauseState));
        }
    }

    public void OnGameEvent(GameFlowEvent flowEvent)
    {
        if (flowEvent.GetAction() == EGameFlowAction.GameOver)
        {
            ChangeNextTransition(HSMTransition.EType.Clear, typeof(GameFlowGameOverState));
        }
    }

    public override void OnExit ()
    {
        this.UnregisterAsListener("Game");
        this.UnregisterAsListener ("Player");
    }
}