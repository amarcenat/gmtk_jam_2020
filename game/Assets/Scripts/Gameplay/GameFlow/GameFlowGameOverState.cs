﻿using UnityEngine;

public class GameOverGameEvent : GameEvent
{
    public GameOverGameEvent() : base("Game")
    {

    }
}

public class GameFlowGameOverState : HSMState
{
    public override void OnEnter()
    {
        UpdaterProxy.Get().SetPause(true);
        Time.timeScale = 0;
        this.RegisterAsListener("Game", typeof(GameFlowEvent));
        new GameOverGameEvent().Push();
    }

    public void OnGameEvent(GameFlowEvent flowEvent)
    {
        switch (flowEvent.GetAction())
        {
            case EGameFlowAction.Quit:
                ChangeNextTransition(HSMTransition.EType.Clear, typeof(GameFlowMenuState));
                break;
            case EGameFlowAction.Retry:
                ChangeNextTransition(HSMTransition.EType.Clear, typeof(GameFlowNormalState));
                break;
        }
    }

    public override void OnExit()
    {
        this.UnregisterAsListener("Game");
        Time.timeScale = 1;
        UpdaterProxy.Get().SetPause(false);
    }
}