﻿using UnityEngine;

public class Score : MonoBehaviour
{
    private float m_Score = 0f;

    public void OnEnable()
    {
        m_Score = 0f;
        this.RegisterToUpdate(true, EUpdatePass.AfterAI);
    }

    public void OnDestroy()
    {
        this.UnregisterToUpdate(EUpdatePass.AfterAI);
    }

    public void UpdateAfterAI()
    {
        m_Score += 0.01f;
    }

    public float GetScore()
    {
        return m_Score;
    }
}
