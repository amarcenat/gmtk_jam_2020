﻿using System.Collections.Generic;
using UnityEngine;

public class HazardSpawner : MonoBehaviour
{
    // Spawning
    // Position relative to the player
    [SerializeField] List<GameObject> m_HazardPrefab = new List<GameObject>();
    [SerializeField] float m_ShipDefaultSpeed;
    [SerializeField] GameObject m_Ship;
    [SerializeField] float m_XMarginRight = 3;
    [SerializeField] float m_YPosSpawn = 10;

    private float m_SpawnTime = 10; // second
    private float m_ShipXSpeed = 0.0f;
    private float m_Time;
    private float m_GameTime = 0.0f;

    // Game Phase
    [SerializeField] float m_TimeBetweenPhase = 20.0f;
    private int m_PhaseIndex = 0;
    private List<int> m_MaxRangeForEachSpawn = new List<int>();
    private List<List<int>> m_Gradation = new List<List<int>>()
    {
        new List<int>(){0, 0 ,0, 0, 100 },
        new List<int>(){75, 0 , 0, 0, 100 },
        new List<int>(){60, 75, 0, 0, 100 },
        new List<int>(){60, 75, 100, 0, 0 },
        new List<int>(){60, 75, 85, 100, 0 },
        new List<int>(){50, 65, 85, 100, 0 }
    };

    private List<float> m_SpawnTimePhase = new List<float>()
    {
        5,
        4.5f,
        4,
        3.5f,
        2.5f,
        1.5f
    };

    void Awake()
    {
        m_Time = 0.0f;
        this.RegisterAsListener("Module", typeof(GameEvent_ActionBeginUse), typeof(GameEvent_ActionStopUse));
        this.RegisterToUpdate(true, EUpdatePass.BeforeAI);
        m_MaxRangeForEachSpawn = m_Gradation[m_PhaseIndex];
        m_SpawnTime = m_SpawnTimePhase[m_PhaseIndex];
    }

    private void OnDestroy()
    {
        this.UnregisterToUpdate(EUpdatePass.BeforeAI);
        this.UnregisterAsListener("Module");
    }

    public void OnGameEvent(GameEvent_ActionBeginUse actionEvent)
    {
        EActionType actionType = actionEvent.GetActionType();
        switch (actionType)
        {
            case EActionType.GoLeft:
                m_ShipXSpeed += m_ShipDefaultSpeed;
                break;
            case EActionType.GoRight:
                m_ShipXSpeed += -1 * m_ShipDefaultSpeed;
                break;
            default:
                break;
        }
    }

    public void OnGameEvent(GameEvent_ActionStopUse actionEvent)
    {
        EActionType actionType = actionEvent.GetActionType();
        switch (actionType)
        {
            case EActionType.GoLeft:
                m_ShipXSpeed -= m_ShipDefaultSpeed;
                break;
            case EActionType.GoRight:
                m_ShipXSpeed -= -1 * m_ShipDefaultSpeed;
                break;
            default:
                break;
        }
    }

    public void UpdateBeforeAI()
    {
        // Time for new phase
        m_GameTime += Time.deltaTime;
        if(m_GameTime > m_TimeBetweenPhase)
        {
            m_PhaseIndex = Mathf.Min(m_PhaseIndex + 1, m_Gradation.Count - 1);
            m_MaxRangeForEachSpawn = m_Gradation[m_PhaseIndex];
            m_SpawnTime = m_SpawnTimePhase[m_PhaseIndex];
            m_GameTime = 0.0f;
        }

        // Time For spawn
        m_Time += Time.deltaTime;
        if(m_Time > m_SpawnTime)
        {
            SpawnHazard();
            m_Time = 0.0f;
        }

        if (m_ShipXSpeed != 0)
        {
            for(int i = 0; i < transform.childCount; ++i)
            {
                GameObject hazardObject = transform.GetChild(i).gameObject;
                if(hazardObject.GetComponent<Hazard>().GetHazardType() == EHazardType.AsteroidStorm)
                {
                    hazardObject.GetComponent<AsteroidStorm>().MoveOnX(m_ShipXSpeed);
                }
                else
                {
                    hazardObject.GetComponent<Hazard>().MoveOnX(m_ShipXSpeed);
                }
            }
        }
    }

    void SpawnHazard()
    {
        Vector3 position = GetSpawnPosition();
        GameObject newHazard = null;
        int indexObjectToSpawn = Random.Range(0, m_HazardPrefab.Count);
        newHazard = Instantiate(m_HazardPrefab[GetHazardIndexToSpawn()], position, Quaternion.identity);
        newHazard.transform.Rotate(Vector3.forward, Random.Range(0, 360));
        EHazardType hazardType = newHazard.GetComponent<Hazard>().GetHazardType();
        Setup(newHazard.GetComponent<Hazard>(), position);
        newHazard.transform.parent = transform;
    }
    

    private Vector3 GetSpawnPosition()
    {
        float posXShipSpeed = 0;
        if(m_ShipXSpeed != 0)
        {
            posXShipSpeed = -1.5f * m_ShipXSpeed;
        }
        float centerSpawnZone = 0;
        if(m_Ship)
        {
            centerSpawnZone = m_Ship.transform.position.x;
        }
        float xPosSpawn = Random.Range(centerSpawnZone - m_XMarginRight + posXShipSpeed, centerSpawnZone + m_XMarginRight + posXShipSpeed);
        return new Vector3(xPosSpawn, m_YPosSpawn, 0);
    }

    private void Setup(Hazard hazard, Vector3 startPos)
    {
        hazard.SetStartPosition(startPos);
        hazard.SetEndPosition(startPos - new Vector3(0, 20, 0));

        //// If we want different speed for each object
        //float modifSpeed = Random.Range(m_ModifSpeed, 1);
        //hazard.SetMovementTime(hazard.GetMovementTime() * modifSpeed);
    }

    private int GetHazardIndexToSpawn()
    {
        int randomResult = Random.Range(0, 100);
        for(int i = 0; i < m_MaxRangeForEachSpawn.Count; ++i)
        {
            if (randomResult < m_MaxRangeForEachSpawn[i])
            {
                return i;
            }
        }

        return 0;
    }
}
