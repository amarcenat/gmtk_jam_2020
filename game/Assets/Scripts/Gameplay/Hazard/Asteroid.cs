﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : Hazard
{
    [SerializeField] int m_Damage = 1;
    [SerializeField] int m_Penetration = 1;
    [SerializeField] AudioClip m_DeathSound;

    private float m_AngleRotation;
    private float m_AspirationTime = 0.0f;
    private bool m_IsAspirated = false;
    private bool m_IsDestoyed = false;
    private Vector3 m_StartingScale;
    private Animator m_Animator;

    public void Start()
    {
        SetHazardType(EHazardType.Asteroid);
        int directionRotation = Random.Range(0, 2);
        m_AngleRotation = (directionRotation == 0 ? -1 : 1) * (0.5f / transform.localScale.x);
        m_StartingScale = transform.localScale;
        m_Animator = GetComponent<Animator>();
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Weapon" && !m_IsDestoyed)
        {
            m_Animator.SetTrigger("Death");
            SoundManagerProxy.Get().PlayMultiple(m_DeathSound);
            Destroy(gameObject, m_Penetration);
            Destroy(collision.gameObject);
            m_IsDestoyed = true;
        }
        else if (collision.gameObject.tag == "Ship" && !m_IsDestoyed)
        {
            m_Animator.SetTrigger("Death");
            SoundManagerProxy.Get().PlayMultiple(m_DeathSound);
            new GameEvent_ShipDamage(m_Damage).Push();
            Destroy(gameObject, m_Penetration);
        }
        else if (collision.gameObject.tag == "Hazard")
        {
            if(collision.gameObject.GetComponent<Hazard>().GetHazardType() == EHazardType.BlackHole)
            {
                GetComponent<CircleCollider2D>().enabled = false;
                m_IsAspirated = true;
                Destroy(gameObject, 5);
            }
        }
    }

    new public void UpdateAI()
    {
        base.UpdateAI();
        transform.Rotate(Vector3.forward, m_AngleRotation);

        if(m_IsAspirated)
        {
            m_AspirationTime += Time.deltaTime;

            transform.localScale = Vector3.Lerp(m_StartingScale, new Vector3(0, 0), m_AspirationTime);
        }
    }
}
