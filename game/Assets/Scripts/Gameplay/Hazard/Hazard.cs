﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine;

public enum EHazardType
{
    Invalid
    , Asteroid
    , BlackHole
    , GeomagneticStorm
    , AsteroidStorm
}

public class Hazard : MonoBehaviour
{
    [SerializeField] float m_LifeTime = 5;

    private EHazardType m_Type;
    private float m_LerpValue;
    private Vector3 m_StartPoint;
    private Vector3 m_EndPoint;


    public void Awake()
    {
        m_LerpValue = 0.0f;
        m_StartPoint = new Vector3(5, 6, 0);
        m_EndPoint = new Vector3(5, -6, 0);
        m_Type = EHazardType.Invalid;
        this.RegisterToUpdate(true, EUpdatePass.AI);
    }

    public void OnDestroy()
    {
        this.UnregisterToUpdate(EUpdatePass.AI);
    }

    public void UpdateAI()
    {
        m_LerpValue += Time.deltaTime / m_LifeTime;

        // Set our position as a fraction of the distance between the markers.
        transform.position = Vector3.Lerp(m_StartPoint, m_EndPoint, m_LerpValue);

        if(m_LerpValue > 1)
        {
            Destroy(gameObject);
        }
    }

    public EHazardType GetHazardType()
    {
        return m_Type;
    }

    public void SetHazardType(EHazardType type)
    {
        m_Type = type;
    }

    public void SetLifeTime(float lifeTime)
    {
        m_LifeTime = lifeTime;
    }

    public float GetLifeTime()
    {
        return m_LifeTime;
    }

    public void SetStartPosition(Vector3 startPosition)
    {
        m_StartPoint = startPosition;
    }

    public Vector3 GetStartPosition()
    {
        return m_StartPoint;
    }

    public void SetEndPosition(Vector3 endingPosition)
    {
        m_EndPoint = endingPosition;
    }

    public Vector3 GetEndPosition()
    {
        return m_EndPoint;
    }

    public void MoveOnX(float speedShip)
    {
        m_StartPoint += new Vector3(Time.deltaTime * speedShip, 0, 0);
        m_EndPoint += new Vector3(Time.deltaTime * speedShip, 0, 0);
    }
}