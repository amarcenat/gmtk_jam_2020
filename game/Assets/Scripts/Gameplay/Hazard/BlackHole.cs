using UnityEngine;

public class BlackHole : Hazard
{

    private float m_AngleRotation;

    public void Start()
    {
        SetHazardType(EHazardType.BlackHole);

        int directionRotation = 1; // Random.Range(0, 2);
        m_AngleRotation = (directionRotation == 0 ? -1 : 1) * 0.5f;
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Ship")
        {
            new GameEvent_ShipDamage(1).Push();
        }
    }
    
    new public void UpdateAI()
    {
        base.UpdateAI();
        transform.Rotate(Vector3.forward, m_AngleRotation);
    }
}