﻿using UnityEngine;

public class GameEvent_MagneticStorm : GameEvent
{
    public GameEvent_MagneticStorm() : base("Module")
    { 
    }
}

public class GeomagneticStorm : Hazard
{
    public void Start()
    {
        SetHazardType(EHazardType.GeomagneticStorm);
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Ship")
        {
            new GameEvent_MagneticStorm().Push();
        }
    }
}
