﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidStorm : Hazard
{
    [SerializeField] GameObject m_AsteroidPrefab;
    [SerializeField] int m_MinAsteroids;
    [SerializeField] int m_MaxAsteroids;
    [SerializeField] int m_Radius;

    // Start is called before the first frame update
    void Start()
    {
        SetHazardType(EHazardType.AsteroidStorm);

        int numberOfAsteroids = Random.Range(m_MinAsteroids, m_MaxAsteroids);
        for(int i = 0; i < numberOfAsteroids; ++i)
        {
            GameObject asteroid = Instantiate(m_AsteroidPrefab, transform.position, Quaternion.identity);
            float sizeAsteroid = Random.Range(0.2f, 0.7f);
            asteroid.transform.localScale = new Vector3(sizeAsteroid, sizeAsteroid, sizeAsteroid);
            asteroid.transform.parent = transform;
            asteroid.transform.Rotate(Vector3.forward, Random.Range(0, 360));
            SetupAsteroid(asteroid.GetComponent<Hazard>());
        }
    }

    private void SetupAsteroid(Hazard hazard)
    {
        int angle = Random.Range(0, 360);
        int distanceCenter = Random.Range(0, m_Radius);
        Vector3 positionAround = new Vector3(distanceCenter * Mathf.Cos(angle), distanceCenter * Mathf.Sin(angle), 0);
        Vector3 startPosition = GetStartPosition() + positionAround;
        hazard.SetStartPosition(startPosition);
        hazard.SetEndPosition(startPosition - new Vector3(0, 20, 0));
    }

    new public void MoveOnX(float speedShip)
    {
        base.MoveOnX(speedShip);
        for (int i = 0; i < transform.childCount; ++i)
        {
            GameObject hazardObject = transform.GetChild(i).gameObject;
            hazardObject.GetComponent<Hazard>().MoveOnX(speedShip);
        }
    }
}
