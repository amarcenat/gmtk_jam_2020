﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class LayeredMusic : MonoBehaviour
{
    [SerializeField] List<AudioClip> m_Layers;
    [SerializeField] Ship m_Ship;
    private string m_ShipEventGUID;
    private uint m_CurrentLayer = 0;

    public void OnEnable()
    {
        m_CurrentLayer = 0;
        SoundManagerProxy.Get().StopAllMusic();
        for (int i = 0; i < m_Layers.Count; ++i)
        {
            SoundManagerProxy.Get().PlayMusic(m_Layers[i], i);
        }
        for (int i = 1; i < m_Layers.Count; ++i)
        {
            SoundManagerProxy.Get().MuteLayer(i);
        }
        m_ShipEventGUID = m_Ship.GetEventGUID();
        this.RegisterAsListener(m_ShipEventGUID, typeof(DamageGameEvent));
    }

    public void OnDestroy()
    {
        this.UnregisterAsListener(m_ShipEventGUID);
        SoundManagerProxy.Get().StopAllMusic();
    }

    public void OnGameEvent(DamageGameEvent shipDamaged)
    {
        m_CurrentLayer++;
        SoundManagerProxy.Get().ToggleLayer((int)m_CurrentLayer, true);
    }
}
