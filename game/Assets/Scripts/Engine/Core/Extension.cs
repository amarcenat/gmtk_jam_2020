﻿
using System.Collections;
using UnityEngine;

public static class Extension
{
    public static void RegisterAsListener (this System.Object objectToNotify, string tag, params System.Type[] GameEventTypes)
    {
        GameEventManagerProxy.Get ().Register (objectToNotify, tag, GameEventTypes);
    }

    public static void UnregisterAsListener (this System.Object objectToNotify, string tag)
    {
        GameEventManagerProxy.Get ().Unregister (objectToNotify, tag);
    }

    public static void RegisterToUpdate (this System.Object objectToNotify, bool isPausable, params EUpdatePass[] updatePassList)
    {
        UpdaterProxy.Get ().Register (objectToNotify, isPausable, updatePassList);
    }
    public static void RegisterToUpdate(this System.Object objectToNotify, params EUpdatePass[] updatePassList)
    {
        UpdaterProxy.Get().Register(objectToNotify, false, updatePassList);
    }

    public static void UnregisterToUpdate (this System.Object objectToNotify, params EUpdatePass[] updatePassList)
    {
        UpdaterProxy.Get ().Unregister (objectToNotify, updatePassList);
    }

    public static void SetX (this Vector3 v, float newX)
    {
        v.Set (newX, v.y, v.z);
    }

    public static void SetY (this Vector3 v, float newY)
    {
        v.Set (v.x, newY, v.z);
    }

    public static void SetZ (this Vector3 v, float newZ)
    {
        v.Set (v.x, v.y, newZ);
    }

    public static void DebugLog (this System.Object caller, System.Object message)
    {
        LoggerProxy.Get ().Log ("[" + caller.ToString() + "]" + message);
    }
    public static void DebugWarning(this System.Object caller, System.Object message)
    {
        LoggerProxy.Get().Warning("[" + caller.ToString() + "]" + message);
    }

    public static Coroutine StartGlobalCoroutine(this System.Object caller, IEnumerator routine)
    {
        if(World.ms_Instance != null)
        {
            return World.ms_Instance.StartCoroutine(routine);
        }
        return null;
    }

    public static void StopGlobalCoroutine(this System.Object caller, Coroutine routine)
    {
        if (World.ms_Instance != null)
        {
            World.ms_Instance.StopCoroutine(routine);
        }
    }

    public static string GetEventGUID(this MonoBehaviour component)
    {
        return component.gameObject.GetInstanceID().ToString();
    }
}