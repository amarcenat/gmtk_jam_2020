﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : ISoundManager
{
    private readonly AudioSource m_EfxSource;
    private readonly List<AudioSource> m_MusicSources;
    private Dictionary<int, Coroutine> m_Routines = new Dictionary<int, Coroutine>();
    private float m_FadeRate = 0.001f;

    public SoundManager (AudioSource efxSource, List<AudioSource> musicSources)
    {
        m_EfxSource = efxSource;
        m_MusicSources = musicSources;
    }

    public void PlaySingle (AudioClip clip)
    {
        if (clip != null)
        {
            m_EfxSource.clip = clip;
            m_EfxSource.Play();
        }
    }

    public void PlayMultiple (AudioClip clip)
    {
        if (clip != null)
        {
            m_EfxSource.PlayOneShot(clip);
        }
    }

    public void PlayMusic (AudioClip clip, int layer)
    {
        if (layer >= m_MusicSources.Count)
        {
            return;
        }
        AudioSource audioSource = m_MusicSources[layer];
        if (audioSource.clip != clip)
        {
            audioSource.clip = clip;
            audioSource.Play ();
            audioSource.loop = true;
        }
    }
    public void MuteLayer(int layer)
    {
        if (layer >= m_MusicSources.Count)
        {
            return;
        }
        AudioSource audioSource = m_MusicSources[layer];
        audioSource.volume = 0;
    }

    public void ToggleLayer(int layer, bool value)
    {
        if (layer >= m_MusicSources.Count)
        {
            return;
        }
        AudioSource audioSource = m_MusicSources[layer];
        Coroutine routine = null;
        if(m_Routines.TryGetValue(layer, out routine))
        {
            this.StopGlobalCoroutine(routine);
        }
        m_Routines.Remove(layer);
        if (value)
        {
            routine = this.StartGlobalCoroutine(FadeLayerIn(audioSource));
        }
        else
        {
            routine = this.StartGlobalCoroutine(FadeLayerOut(audioSource));
        }
        m_Routines.Add(layer, routine);
    }

    public void StopAllMusic()
    {
        foreach(AudioSource source in m_MusicSources)
        {
            if (source != null)
            {
                source.Stop();
                source.clip = null;
            }
        }
        foreach(Coroutine routine in m_Routines.Values)
        {
            this.StopGlobalCoroutine(routine);
        }
        m_Routines.Clear();
    }

    IEnumerator FadeLayerIn(AudioSource audioSource)
    {
        while (audioSource.volume < 1)
        {
            audioSource.volume += m_FadeRate;
            yield return null;
        }
    }

    IEnumerator FadeLayerOut(AudioSource audioSource)
    {
        while (audioSource.volume > 0)
        {
            audioSource.volume -= m_FadeRate;
            yield return null;
        }
    }
}