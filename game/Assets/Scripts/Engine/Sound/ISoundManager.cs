﻿using UnityEngine;

public interface ISoundManager
{
    void PlaySingle(AudioClip clip);
    void PlayMultiple(AudioClip clip);
    void PlayMusic(AudioClip clip, int layer);
    void ToggleLayer(int layer, bool value);
    void MuteLayer(int layer);
    void StopAllMusic();
}

public class SoundManagerProxy : UniqueProxy<ISoundManager>
{ }