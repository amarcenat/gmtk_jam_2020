﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace _2DGameToolKitTest
{
    using UnityAssertionExeption = UnityEngine.Assertions.AssertionException;

    public class UpdaterTest
    {
        private readonly Updater m_Updater = new Updater();

        class DummyAILogic
        {
            public DummyAILogic()
            {
                m_UpdateCount = 0;
            }

            public void UpdateAI()
            {
                m_UpdateCount++;
            }

            public void UpdateLast()
            {
                // This should not be allowed in an update callback
                this.UnregisterToUpdate(EUpdatePass.Last);
            }

            public int m_UpdateCount;
        }

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            UnityEngine.Assertions.Assert.raiseExceptions = true;
            UpdaterProxy.Open(m_Updater);
        }

        [TearDown]
        public void TearDown()
        {
            m_Updater.Reset();
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            m_Updater.Reset();
            UpdaterProxy.Close(m_Updater);
        }

        [Test]
        public void TestRegisterToUpdate()
        {
            DummyAILogic objectToNotify = new DummyAILogic();
            objectToNotify.RegisterToUpdate(EUpdatePass.AI);

            m_Updater.Update();
            Assert.IsTrue(objectToNotify.m_UpdateCount == 1);

            m_Updater.Update();
            Assert.IsTrue(objectToNotify.m_UpdateCount == 2);
        }

        [Test]
        public void TestUnregisterToUpdate()
        {
            DummyAILogic objectToNotify = new DummyAILogic();
            objectToNotify.RegisterToUpdate(EUpdatePass.AI);

            m_Updater.Update();
            Assert.IsTrue(objectToNotify.m_UpdateCount == 1);

            objectToNotify.UnregisterToUpdate(EUpdatePass.AI);
            m_Updater.Update();
            Assert.IsTrue(objectToNotify.m_UpdateCount == 1);
        }

        [Test]
        public void TestNoReentrancy()
        {
            DummyAILogic objectToNotify = new DummyAILogic();
            objectToNotify.RegisterToUpdate(EUpdatePass.Last);

            Assert.Throws<UnityAssertionExeption>(delegate { m_Updater.Update(); });
        }
    }
}
