﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace _2DGameToolKitTest
{
    public class HSMTest
    {
        class DummyStateBase : HSMState
        {
            public override void OnEnter ()
            {
                m_IsInStack = true;
            }

            public override void OnExit ()
            {
                m_IsInStack = false;
            }

            public bool m_IsInStack;
        }

        class DummyState : DummyStateBase
        {
            public override void OnEnter ()
            {
                base.OnEnter();
                ChangeNextTransition (HSMTransition.EType.Child, typeof (DummyStateChild));
            }
        }

        class DummyStateExit : DummyStateBase
        {
            public override void OnEnter()
            {
                base.OnEnter();
                ChangeNextTransition(HSMTransition.EType.Exit);
            }
        }

        class DummyStateChild : DummyStateBase
        {
            public override void OnEnter ()
            {
                base.OnEnter ();
                ChangeNextTransition (HSMTransition.EType.Siblings, typeof (DummyStateSibling));
            }
        }

        class DummyStateSibling : DummyStateBase
        {
            public override void OnEnter ()
            {
                base.OnEnter ();
                ChangeNextTransition (HSMTransition.EType.Clear, typeof (DummyState));
            }
        }

        class DummyHSM : HSM
        {
            public DummyHSM ()
               : base (new DummyState ()
                     , new DummyStateExit()
                     , new DummyStateChild ()
                     , new DummyStateSibling ()
               )
            { }
        }

        private bool CheckIsInStack<T>() where T : DummyStateBase
        {
            return ((T)m_States[typeof(T)]).m_IsInStack;
        }

        private void PerformHSMUpdate(int numberOfSteps)
        {
            for(int i = 0; i < numberOfSteps; i++)
            {
                m_HSM.UpdateLast();
            }
        }

        private Dictionary<System.Type, HSMState> m_States;
        private readonly DummyHSM m_HSM = new DummyHSM();
        readonly NullUpdater m_Updater = new NullUpdater();
        readonly NullLogger m_Logger = new NullLogger();

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            UpdaterProxy.Open(m_Updater);
            LoggerProxy.Open(m_Logger);
            m_States = m_HSM.GetStates();
        }

        [TearDown]
        public void TearDown()
        {
            m_HSM.Stop();
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            LoggerProxy.Close(m_Logger);
            UpdaterProxy.Close(m_Updater);
        }

        [Test]
        public void TestStart ()
        {
            Assert.IsTrue (m_HSM.IsEmpty ());
            m_HSM.Start (typeof (DummyState));
            HSMState[] currentStack = m_HSM.GetStack ();
            Assert.IsTrue (currentStack.Length == 1);
            Assert.IsTrue (currentStack[0].GetID () == typeof (DummyState));
            Assert.IsTrue (CheckIsInStack<DummyState> ());
        }

        [Test]
        public void TestChildTransition ()
        {
            m_HSM.Start (typeof (DummyState));
            PerformHSMUpdate (1);
            HSMState[] currentStack = m_HSM.GetStack ();
            Assert.IsTrue (currentStack.Length == 2);
            Assert.IsTrue (currentStack[1].GetID () == typeof (DummyState));
            Assert.IsTrue (CheckIsInStack<DummyState> ());
            Assert.IsTrue (currentStack[0].GetID () == typeof (DummyStateChild));
            Assert.IsTrue (CheckIsInStack<DummyStateChild> ());
        }

        [Test]
        public void TestSiblingTransition ()
        {
            m_HSM.Start (typeof (DummyState));
            PerformHSMUpdate (2);
            HSMState[] currentStack = m_HSM.GetStack ();
            Assert.IsTrue (currentStack.Length == 2);
            Assert.IsTrue (currentStack[1].GetID () == typeof (DummyState));
            Assert.IsTrue (CheckIsInStack<DummyState> ());
            Assert.IsTrue (currentStack[0].GetID () == typeof (DummyStateSibling));
            Assert.IsTrue (CheckIsInStack<DummyStateSibling> ());
            Assert.IsFalse (CheckIsInStack<DummyStateChild> ());
            Assert.IsFalse (CheckIsInStack<DummyStateExit>());
        }

        [Test]
        public void TestClearTransition ()
        {
            m_HSM.Start (typeof (DummyState));
            PerformHSMUpdate (3);
            HSMState[] currentStack = m_HSM.GetStack ();
            Assert.IsTrue (currentStack.Length == 1);
            Assert.IsTrue (currentStack[0].GetID () == typeof (DummyState));
            Assert.IsTrue (CheckIsInStack<DummyState> ());
            Assert.IsFalse (CheckIsInStack<DummyStateExit>());
            Assert.IsFalse (CheckIsInStack<DummyStateChild> ());
            Assert.IsFalse (CheckIsInStack<DummyStateSibling> ());
        }

        [Test]
        public void TestExitTransition ()
        {
            m_HSM.Start (typeof (DummyStateExit));
            PerformHSMUpdate (4);
            HSMState[] currentStack = m_HSM.GetStack ();
            Assert.IsTrue (currentStack.Length == 0);
            Assert.IsFalse (CheckIsInStack<DummyState> ());
            Assert.IsFalse (CheckIsInStack<DummyStateExit>());
            Assert.IsFalse (CheckIsInStack<DummyStateChild> ());
            Assert.IsFalse (CheckIsInStack<DummyStateSibling> ());
        }

        [Test]
        public void TestStop ()
        {
            m_HSM.Start (typeof (DummyState));
            PerformHSMUpdate (3);
            m_HSM.Stop ();
            HSMState[] currentStack = m_HSM.GetStack ();
            Assert.IsTrue (currentStack.Length == 0);
            Assert.IsFalse (CheckIsInStack<DummyState> ());
            Assert.IsFalse (CheckIsInStack<DummyStateExit>());
            Assert.IsFalse (CheckIsInStack<DummyStateChild> ());
            Assert.IsFalse (CheckIsInStack<DummyStateSibling> ());
        }
    }
}
