﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace _2DGameToolKitTest
{
    using UnityAssertionExeption = UnityEngine.Assertions.AssertionException;

    public class UniqueProxyTest
    {
        class DummyClass
        { }

        class DummyClassProxy : UniqueProxy<DummyClass>
        { }

        private readonly DummyClass m_Dummy = new DummyClass();

        [SetUp]
        public void SetUp()
        {
            UnityEngine.Assertions.Assert.raiseExceptions = true;
            DummyClassProxy.Open(m_Dummy);
        }

        [TearDown]
        public void TearDown()
        {
            if (DummyClassProxy.IsValid())
            {
                DummyClassProxy.Close(m_Dummy);
            }
        }

        [Test]
        public void TestOpen()
        {
            Assert.IsTrue(DummyClassProxy.IsValid());
            Assert.AreEqual(m_Dummy, DummyClassProxy.Get());
        }

        [Test]
        public void TestCloseAfterOpen()
        {
            DummyClassProxy.Close(m_Dummy);
            Assert.Throws<UnityAssertionExeption>(delegate { DummyClassProxy.Get(); });
        }

        [Test]
        public void TestCloseBeforeOpen()
        {
            DummyClassProxy.Close(m_Dummy);
            Assert.Throws<UnityAssertionExeption>(delegate { DummyClassProxy.Close(m_Dummy); });
        }

        [Test]
        public void TestDoubleOpen()
        {
            Assert.Throws<UnityAssertionExeption>(delegate { DummyClassProxy.Open(m_Dummy); });
        }

        public void TestCloseWithWrongProxy()
        {
            DummyClass otherDummy = new DummyClass();
            Assert.Throws<UnityAssertionExeption>(delegate { DummyClassProxy.Close(otherDummy); });
        }
    }
}
